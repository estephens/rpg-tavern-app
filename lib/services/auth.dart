import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:rpg_tavern_app/models/user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rpg_tavern_app/services/database.dart';

class AuthService {

  final FirebaseAuth _auth = FirebaseAuth.instance;

  // create user obj based on firebase user
  User _userFromFirebaseUser(FirebaseUser user) {
    return user != null ? User(uid: user.uid, isVerified: user.isEmailVerified) : null;
  }

  // auth change user stream
  Stream<User> get user {
    return _auth.onAuthStateChanged
      //.map((FirebaseUser user) => _userFromFirebaseUser(user));
      .map(_userFromFirebaseUser);
  }

  // sign in anon
  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // sign in with email and password
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      var exception  = e as PlatformException;
      return exception.message;
    }
  }


  // register with email and password
  Future registerWithEmailAndPassword(String email, String password, String username, int color, String avatar) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(email: email, password:password);
      FirebaseUser user= result.user;
      await user.sendEmailVerification();
      await DatabaseService(uid: user.uid).updateUserData(username, color, avatar);
      return _userFromFirebaseUser(user);
    } catch (e) {
      var exception  = e as PlatformException;
      return exception.message;
    }
  }

  // sign out
  Future signOut() async{
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //forgot password
  Future<void> resetPassword(String email) async {
    await _auth.sendPasswordResetEmail(email: email);
  }

  Future<void> deleteAccount() async {
    try{
      FirebaseUser _firebaseUser = await _auth.currentUser();
      await _firebaseUser.delete();
      await DatabaseService(uid: _firebaseUser.uid).deleteUserData();
      List<QuerySnapshot> snapshots = await DatabaseService(uid: _firebaseUser.uid).messageCollection.where('uid', isEqualTo:_firebaseUser.uid).snapshots().toList();
      snapshots.forEach((snap) => snap.documents.forEach((doc) async {
        if(doc.data['uid'] == _firebaseUser.uid){
          await doc.reference.delete();
        }
      }));
    }catch(e){
      print(e.toString());
      return e.toString();
    }
  }

}
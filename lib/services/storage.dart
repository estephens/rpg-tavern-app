import 'dart:io';
import 'package:path/path.dart' as Path;
import 'package:firebase_storage/firebase_storage.dart';

class StorageService{
  final _storage = FirebaseStorage.instance;

  final _gamesHeader = FirebaseStorage.instance.ref().child("/Games/Header/");
  final _gamesIcon = FirebaseStorage.instance.ref().child("/Games/Icon/");
  final _users = FirebaseStorage.instance.ref().child("/Users/");
  final _characters = FirebaseStorage.instance.ref().child("/Characters/");

  Future<String> addOrUpdateGameHeader(File image, String fileName, {String currentFileUrl}) async {
    if(currentFileUrl.isNotEmpty) await deleteImage(currentFileUrl);
    if(image != null){
      String imageName = fileName + Path.extension(image.path);
      StorageReference ref = _gamesHeader.child(imageName);
      StorageUploadTask task = ref.putFile(image);
      String url = await (await task.onComplete).ref.getDownloadURL();
      return url;
    }
    else  return '';
  }

  Future<String> addOrUpdateGameIcon(File image, String fileName, {String currentFileUrl}) async {
   if(currentFileUrl.isNotEmpty) deleteImage(currentFileUrl);
   if(image != null){
      String imageName = fileName + Path.extension(image.path);
      StorageReference ref = _gamesIcon.child(imageName);
      StorageUploadTask task = ref.putFile(image);
      String url = await (await task.onComplete).ref.getDownloadURL();
      return url;
    }
    else return '';
  }

  Future<String> addOrUpdateUser(File image, String fileName, {String currentFileUrl}) async {
    await deleteImage(currentFileUrl);
    String imageName = fileName + Path.extension(image.path);
    StorageReference ref = _users.child(imageName);
    StorageUploadTask task = ref.putFile(image);
    String url = await (await task.onComplete).ref.getDownloadURL();
    return url;
  }

  Future<String> addOrUpdateCharacter(File image, String fileName, {String currentFileUrl}) async {
    await deleteImage(currentFileUrl);
    String imageName = fileName + Path.extension(image.path);
    StorageReference ref = _characters.child(imageName);
    StorageUploadTask task = ref.putFile(image);
    String url = await (await task.onComplete).ref.getDownloadURL();
    return url;
  }

  Future deleteImage(String currentFileUrl) async {
    if(currentFileUrl.isNotEmpty && currentFileUrl != null){
      final _oldRef = await _storage.getReferenceFromUrl(currentFileUrl);
      if(_oldRef != null){
        _oldRef.delete();
      }
    }
    return;
  }
}
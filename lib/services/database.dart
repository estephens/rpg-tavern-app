import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:rpg_tavern_app/models/game.dart';
import 'package:rpg_tavern_app/models/user.dart';
import 'package:rpg_tavern_app/models/message.dart';

class DatabaseService {

  final String uid;
  DatabaseService({this.uid});
  //collection reference
  final CollectionReference userCollection = Firestore.instance.collection('users');
  final CollectionReference characterCollection = Firestore.instance.collection('characters');
  final CollectionReference messageCollection = Firestore.instance.collection('messages');
  final CollectionReference gameCollection = Firestore.instance.collection('games');

  //USER METHODS
  //update user data
  Future updateUserData(String username, int colorData, String avatar) async {
    return await userCollection.document(uid).setData(
      {
        'uid': uid,
        'username': username,
        'color': colorData,
        'avatar': avatar
      }
    );
  }

  //userdata from Document snapshot
  UserData _userDataFromSnapshot(DocumentSnapshot snapshot){
    return UserData(
      uid: snapshot.data['uid'] ?? "",
      username: snapshot.data['username'] ?? "",
      colorData: snapshot.data['color'] as int ?? Colors.blue.value.toInt(),
      avatar: snapshot.data['avatar'] ?? ""
    );
  }

  //userdata fromsnapshot

  Future deleteUserData() async {
    return await userCollection.document(uid).delete();
  }

  Future<UserData> getUser(String userId) async {
    var _documentReference = userCollection.document(userId);
    var snapshot = await _documentReference.snapshots().last;
    return _userDataFromSnapshot(snapshot);
  }

  //get user doc stream
  Stream<UserData> getUserData(String uid){
    return userCollection.document(uid).snapshots().map((snapshot) => _userDataFromSnapshot(snapshot));
  }

  //get user doc stream
  Stream<UserData> get userData {
    return userCollection.document(uid).snapshots().map((snapshot) => _userDataFromSnapshot(snapshot));
  }
  Stream<List<UserData>> usersInList (List<String> uids){
    return userCollection.where('uid', whereIn: uids).snapshots().map((snapshot){
      return snapshot.documents.map((doc) => _userDataFromSnapshot(doc)).toList();
    });
  }

  //CHARACTER METHODS
  Future updateCharacter(String name, String race, String cClass, String gender, int age) async {
    return await characterCollection.document(uid).setData(
      {
        'name': name,
        'race': race,
        'cClass': cClass,
        'gender': gender,
        'age': age
      }
    );
  }

  Future deleteCharacter(String cid) async {
    return await characterCollection.document(cid).delete();
  }

  //MESSAGE METHODS
  //update character data
  Future addMessage(String uid, String action, String text, DateTime time, bool edited) async {
    return await messageCollection.add(
      {
        'uid': uid,
        'action': action,
        'text': text,
        'time': time,
        'editedAt': time,
        'edited': edited
      }
    );
  }

  Future updateMessage(String mid, String uid, String action, String text, DateTime time, DateTime editedAt, bool edited) async {
    return await messageCollection.document(mid).setData(
      {
        'uid': uid,
        'action': action,
        'text': text,
        'time': time,
        'editedAt': editedAt,
        'edited': edited
      }
    );
  }

  Future deleteMesssage(String mid) async {
    return await messageCollection.document(mid).delete();
  }

  //message list from snapshot
  List<Message> _messageListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc){
      return Message(
        mid: doc.documentID,
        uid: doc.data['uid'],
        action: doc.data['action'],
        text: doc.data['text'],
        time: doc.data['time'].toDate(),
        editedAt: doc.data['editedAt'].toDate(),
        edited: doc.data['edited'],
        user: null
      );
    }).toList();
  }

  //get messages stream
  Stream<List<Message>> get messages{
     return messageCollection.orderBy('time', descending: true).snapshots().map((snapshot) =>  _messageListFromSnapshot(snapshot));
  }


  //GAME DATA
  Future<DocumentReference> addGame(String ownerId, String name, String description, String system, bool public, int maxPlayers) async {
    DocumentReference newGame = await gameCollection.add(
      {
        'ownerid': ownerId,
        'name': name,
        'description': description,
        'system': system,
        'status': 'Pending',
        'public': public,
        'maxPlayers': maxPlayers,
        'icon': '',
        'header': '',
        'players':[uid],
        'channels':[],
        'attributes':[]
      }
    );
    await newGame.collection('attributes').add(
      {
        'name':'Strength',
        'skills': [
          'Athletics',
        ]
      }
    );
    await newGame.collection('attributes').add(
      {
        'name':'Dexterity',
        'skills': [
          'Acrobatics',
          'Sleight of Hand',
          'Stealth',
        ]
      }
    );
    await newGame.collection('attributes').add(
      {
        'name':'Constitution',
        'skills': []
      }
    );
    await newGame.collection('attributes').add(
      {
        'name':'Intelligence',
        'skills': [
          'Arcana',
          'History',
          'Investigation',
          'Nature',
          'Religion',
        ]
      }
    );
    await newGame.collection('attributes').add(
      {
        'name':'Wisdom',
        'skills': [
          'Animal Handling',
          'Insight',
          'Medicine',
          'Perception',
          'Survival'
        ]
      }
    );
    await newGame.collection('attributes').add(
      {
        'name':'Charisma',
        'skills': [
          'Deception',
          'Intimidation',
          'Performance',
          'Persuasion'
        ]
      }
    );
    await newGame.collection('channels').add({'name':'Chat', 'order': 0});
    return newGame;
  }

  Future updateGame(String gid, String ownerId, String name, String description, String system, String status, bool public, int maxPlayers, String icon, String header, List<String> players) async {
    return await gameCollection.document(gid).updateData(
      {
        'ownerid': ownerId,
        'name': name,
        'description': description,
        'system': system,
        'status': status,
        'public': public,
        'maxPlayers': maxPlayers,
        'icon': icon,
        'header': header,
        'players': players,
      }
    );
  }

  Future deleteGame(String gid) async {
    return await gameCollection.document(gid).delete();
  }

  //message list from snapshot
  List<Game> _gameListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc){
      return Game(
        gid: doc.documentID,
        ownerId: doc.data['ownerid'],
        name: doc.data['name'],
        description: doc.data['description'],
        system: doc.data['system'],
        status: doc.data['status'],
        public: doc.data['public'],
        maxPlayers: doc.data['maxPlayers'],
        icon: doc.data['icon'],
        header: doc.data['header'],
        users: doc.data['players'],
        channels: doc.data['channels'],
        attributes: doc.data['attributes']
      );
    }).toList();
  }

  Future<Game> getGame(String gid) async {
    DocumentReference _gameRef =  gameCollection.document(gid);
     DocumentSnapshot doc = await  _gameRef.get();
     return Game(
      gid: doc.documentID,
      ownerId: doc.data['ownerid'],
      name: doc.data['name'],
      description: doc.data['description'],
      system: doc.data['system'],
      status: doc.data['status'],
      public: doc.data['public'],
      maxPlayers: doc.data['maxPlayers'],
      icon: doc.data['icon'],
      header: doc.data['header'],
      users: doc.data['players'],
      channels: doc.data['channels'],
      attributes: doc.data['attributes']
    );
  }

  //get messages stream
  Stream<List<Game>> get games {
    return gameCollection.where('players', arrayContains:uid).snapshots().map((snapshot) =>  _gameListFromSnapshot(snapshot));
  }

  //GAME ATTRIBUTE FUNCTIONS
  //UPDATE ATTRIBUTE
  Future<DocumentReference> addAttribute(String gid, String name, List skills) async {
    DocumentReference attrRef =  await gameCollection.document(gid).collection('attributes').add(
      {
        'name': name,
        'skills': skills,
      }
    );
    return attrRef;
  }

  //UPDATE ATTRIBUTE
  Future<DocumentReference> updateAttribute(String gid, String aid, String name, List skills) async {
    DocumentReference attrRef = gameCollection.document(gid).collection('attributes').document(aid);
    await attrRef.updateData(
      {
        'aid': aid,
        'name': name,
        'skills': skills,
      }
    );
    return attrRef;
  }

  //DELETE ATTRIBUTE
  Future deleteAttribute(String gid, String aid) async {
    return await gameCollection.document(gid).collection('attributes').document(aid).delete();
  }

  //GAME CHANNEL FUNCTIONS
  //UPDATE CHANNEL
  Future<DocumentReference> addChannel(String gid, String name, int order) async {
    DocumentReference channelRef =  await gameCollection.document(gid).collection('channels').add(
      {
        'name': name,
        'order': order
      }
    );
    return channelRef;
  }

  //UPDATE CHANNEL
  Future<DocumentReference> updateChannel(String gid, String cid, String name, int order) async {
    DocumentReference channelRef = gameCollection.document(gid).collection('channels').document(cid);
    await channelRef.updateData(
      {
        'cid': cid,
        'name': name,
        'order': order,
      }
    );
    return channelRef;
  }

  //DELETE CHANNEL
  Future deleteChannel(String gid, String cid) async {
    return await gameCollection.document(gid).collection('channels').document(cid).delete();
  }

  
}
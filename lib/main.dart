import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rpg_tavern_app/screens/wrapper.dart';
import 'package:rpg_tavern_app/services/auth.dart';
import 'package:rpg_tavern_app/models/user.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Colors.teal,
          accentColor: Colors.grey[500],
          backgroundColor: Colors.white,
          textTheme: TextTheme(
            title: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            subtitle: TextStyle(color: Colors.black87, fontSize: 18) ,
            headline: TextStyle(color: Colors.black54),
            display1: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            display2: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            display3: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          )
        ),
        home: Wrapper(),
      ),
    );
  }
}

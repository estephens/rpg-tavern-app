import 'package:flutter/material.dart';

class User {

  final String uid;
  final bool isVerified;

  User({this.uid, this.isVerified});

}

class UserData {

  final String uid;
  final String username;
  final int colorData;
  final String avatar;

  UserData({this.uid, this.username, this.colorData, this.avatar});

  Color color(){
    return new Color(this.colorData);
  }
}
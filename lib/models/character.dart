class Character{
  final String cid;
  final String uid;
  final String gid;
  String name;
  String race;
  String cClass;
  String gender;
  int level;
  List<Attribute> attributes;
  List<Skill> skills;
  List<Save> saves;

  Character({
    this.cid,
    this.uid,
    this.gid,
    this.name,
    this.race,
    this.cClass,
    this.level,
  });
}

class Attribute{
  final String avid;
  final String aid;
  int value;

  Attribute({
    this.avid,
    this.aid,
    this.value
  });

  int mod(){
    return((this.value.toInt()-10)/2).floor();
  }
}

class Skill{
  final String sid;
  String name;
  bool proficient;
  Attribute attribute;
  int bonus;

  Skill({
    this.sid,
    this.name,
    this.proficient,
    this.attribute,
    this.bonus
  });

  int val(int level){
    return attribute.mod() + (this.proficient ? (1 + level/4).ceil() : 0) + bonus;
  }
}

class Save{
  final String sid;
  Attribute atribute;
  bool proficient;
  int bonus;

  Save({
    this.sid,
    this.atribute,
    this.proficient,
    this.bonus
  });
}
import 'package:intl/intl.dart';
import 'package:rpg_tavern_app/models/user.dart';

class Message {
  final String mid;
  final String uid;
  String action;
  String text;
  DateTime time;
  DateTime editedAt;
  bool edited;
  UserData user;

  Message({
    this.mid,
    this.uid,
    this.action,
    this.text,
    this.time,
    this.editedAt,
    this.edited,
    this.user
  });

  String footer (){
    var date = new DateFormat.yMd().add_jm().format(this.editedAt);
    return this.edited ? "Edited: " + date : date;
  }
}
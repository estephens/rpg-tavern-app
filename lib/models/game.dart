import 'package:intl/intl.dart';
import 'package:rpg_tavern_app/models/character.dart';

class Game {
  final String gid;
  final String ownerId;
  String name;
  String description;
  String system;
  String status;
  bool public;
  int maxPlayers;
  String icon;
  String header;
  List users;
  List channels;
  List attributes;

  Game({
    this.gid,
    this.ownerId,
    this.name,
    this.description,
    this.system,
    this.status,
    this.public,
    this.maxPlayers,
    this.icon,
    this.header,
    this.users,
    this.channels,
    this.attributes
  });
}

class Channel{
  final String cid;
  String name;
  int order;

  Channel({
    this.cid,
    this.name,this.order,
  });

}

class GameAttribute{
  final String aid;
  String name;
  List skills;

  GameAttribute({this.aid, this.name, this.skills});
}

class Post {
  final String pid;
  final String cid;
  final String plid;
  final String chid;
  String action;
  String text;
  DateTime time;
  DateTime editedAt;
  bool edited;
  Character character;

  Post({
    this.pid,
    this.cid,
    this.plid,
    this.chid,
    this.action,
    this.text,
    this.time,
    this.editedAt,
    this.edited,
    this.character
  });

  String footer (){
    var date = new DateFormat.yMd().add_jm().format(this.editedAt);
    return this.edited ? "Edited: " + date : date;
  }
}

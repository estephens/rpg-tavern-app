import 'package:dashed_container/dashed_container.dart';
import 'package:flutter/material.dart';

class OptionalAvatar extends StatelessWidget {
  final String name;
  final int color;

  OptionalAvatar({this.name, this.color});

  @override
  Widget build(BuildContext context) {
    if(name.isNotEmpty){
      return CircleAvatar(
        radius: 35.0,
        backgroundColor: Color(color),
        child: Text(
          name.substring(0,1).toUpperCase(),
          style: TextStyle(
            color: Colors.white,
            fontSize: 35.0
          )
        ),
      );
    }
    else{
      return DashedContainer(
        dashColor: Colors.grey[500],
        dashedLength: 10.0,
        blankLength: 2.5,
        strokeWidth: 3.0,
        borderRadius: 30.0,
        boxShape: BoxShape.rectangle,
        child: SizedBox(
          height: 60,
          width: 60,
          child: Center(
            child: Text(
              "Icon",
              style: TextStyle(
                color: Colors.grey[600],
                fontSize: 20.0
              ),
            )
          ),
        )
      );
    }
  }
}
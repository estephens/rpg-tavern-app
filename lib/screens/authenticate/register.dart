import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rpg_tavern_app/models/user.dart';
import 'package:rpg_tavern_app/services/auth.dart';
import 'package:rpg_tavern_app/services/database.dart';
import 'package:rpg_tavern_app/services/storage.dart';
import 'package:rpg_tavern_app/shared/avatar.dart';
import 'package:rpg_tavern_app/shared/constants.dart';
import 'package:rpg_tavern_app/shared/loading.dart';

class Register extends StatefulWidget {

  final Function toggleView;
  Register({this.toggleView});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  
  final AuthService _auth = AuthService();

  //text field state
  String email = '';
  String password = '';
  String confirmPassword = '';
  String username = '';
  String avatar = '';
  File avatarFile;
  int color;
  Color avatarColor = Colors.deepOrangeAccent;
  String error = '';
  bool loading = false;
  final _formKey = GlobalKey<FormState>();

  ScrollController _scrollController = new ScrollController();
  double _currentPosition = 0;
  
  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    super.initState();
  }

  void _scroll(double direction){
    double screenWidth = MediaQuery.of(context).size.width * direction;
    _scrollController.animateTo(_currentPosition + screenWidth, duration:Duration(milliseconds: 1000), curve: Curves.easeOutQuint);
    setState(() => _currentPosition = _currentPosition + screenWidth);
    return;
  }

  void _scrollListener(){
    setState(() => _currentPosition = _scrollController.offset);
  }

  //avatar picker
  Future chooseAvatar() async { 
    File image = await ImagePicker.pickImage(source: ImageSource.gallery, maxWidth: 250);
    setState(() {
      avatarFile = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    return loading ? Loading() : Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
        title: Text('RPG Tavern'),
        actions: <Widget>[
          FlatButton.icon(
            icon: Icon(Icons.person, color: Colors.white,),
            label: Text(
              "Sign In",
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () => widget.toggleView(),
          )
        ],
      ),
      body: Center(
        child: Container(
          alignment: Alignment.center,
          width: MediaQuery.of(context).size.width < 500 ? double.infinity: 500,
          color: Theme.of(context).backgroundColor,
          child:Form(
            key: _formKey,
            child: ListView(
              controller: _scrollController,
              physics: PageScrollPhysics(),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(vertical:0, horizontal: 15.0),
                  child: Center(
                    child: ListView(
                      shrinkWrap: true,
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Register", style: Theme.of(context).textTheme.headline,),
                        SizedBox(height: 10,),
                        TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          onChanged: (value) => setState(() => email = value),
                          validator: (value) => value.isEmpty ? "Enter an E-mail." : null,
                          decoration: TextInputDecoration.copyWith(labelText: "Email"),
                        ),
                        SizedBox(height: 20,),
                        TextFormField(
                          obscureText: true,
                          onChanged: (value) => setState(()=> password = value),
                          validator: (value) => value.length < 6 ? "Enter a password 6+ characters long." : null,
                          decoration: TextInputDecoration.copyWith(labelText: "Password"),
                        ),
                        SizedBox(height: 20,),
                        TextFormField(
                          obscureText: true,
                          onChanged: (value) => setState(()=> confirmPassword = value),
                          validator: (value) => value != password ? "Passwords did not match." : null,
                          decoration: TextInputDecoration.copyWith(labelText: "Confirm Password"),
                        ),
                        SizedBox(height:20),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            RaisedButton(
                              child: Text("Next", style: TextStyle(color: Colors.white),),
                              elevation: 10.0,
                              color: Theme.of(context).accentColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20.0))
                              ),
                              onPressed: () => _scroll(1.0),
                            ),
                          ],
                        ),
                        Text(error,style: TextStyle(color: Colors.red),)
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(vertical:0, horizontal: 15.0),
                  child: Center(
                    child: ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        Text("Avatar Settings", style: Theme.of(context).textTheme.headline,),
                        SizedBox(height: 10,),
                        TextFormField(
                          onChanged: (value) => setState(()=> username = value),
                          validator: (value) => value.length < 4 ? "Enter a username 4+ characters long." : null,
                          decoration: TextInputDecoration.copyWith(labelText: "Username"),
                        ),
                        SizedBox(height: 20.0),
                        avatarFile == null ?
                        Column(
                          children: <Widget>[
                            ButtonTheme(
                              minWidth: double.infinity,
                              child: RaisedButton.icon(
                                label: Text("Upload Avatar"),
                                color: Colors.white,
                                icon: Icon(Icons.photo_library),
                                elevation: 10,
                                onPressed: chooseAvatar,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0)
                                ),
                              ),
                            ),
                            SizedBox(height: 5,),
                            OptionalAvatar(
                              name: username ?? "New User",
                              color: Theme.of(context).primaryColor.value
                            ),
                          ],
                        )
                        : Column(
                          children: <Widget>[
                            ButtonTheme(
                              minWidth: double.infinity,
                              child: RaisedButton.icon(
                                label: Text("Remove Avatar"),
                                color: Colors.white,
                                icon: Icon(Icons.close),
                                elevation: 10,
                                onPressed: () => setState(()=> avatarFile = null),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0)
                                ),
                              ),
                            ),
                            SizedBox(height: 5,),
                            CircleAvatar(
                              backgroundColor: Theme.of(context).backgroundColor,
                              backgroundImage: Image.file(avatarFile).image,
                              radius: 30,
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            RaisedButton(
                              child: Text("Previous"),
                              elevation: 10,
                              color: Theme.of(context).backgroundColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20.0))
                              ),
                              onPressed: () => _scroll(-1.0),
                            ),
                            RaisedButton(
                              child: Text("Register",style: TextStyle(color: Colors.white),),
                              elevation: 10,
                              color: Theme.of(context).accentColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20.0))
                              ),
                              onPressed: () async {
                                if(_formKey.currentState.validate()){
                                  setState(() => loading = true);
                                  dynamic result = await _auth.registerWithEmailAndPassword(
                                    email, 
                                    password, 
                                    username ?? "new user", 
                                    Theme.of(context).primaryColor.value, 
                                    avatar ?? ""
                                  );
                                  if(result is String){
                                    setState(() {
                                      loading = false;
                                      error = result;
                                    });
                                  }
                                  else if(result is User){
                                    String url = await StorageService().addOrUpdateUser(avatarFile, result.uid, currentFileUrl: "");
                                    UserData userData = await DatabaseService(uid: result.uid).getUserData(result.uid).first;
                                    await DatabaseService(uid: result.uid).updateUserData(userData.username, userData.colorData, url);
                                  }
                                }
                                else{
                                  _scroll(-1.0);
                                }
                              },
                            ),
                          ],
                        ),
                        Text(error,style: TextStyle(color: Colors.red),)
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      )
    );
  }
}
import 'package:flutter/rendering.dart';
import 'package:rpg_tavern_app/services/auth.dart';
import 'package:flutter/material.dart';
import 'package:rpg_tavern_app/shared/constants.dart';
import 'package:rpg_tavern_app/shared/loading.dart';

class SignIn extends StatefulWidget {

  final Function toggleView;
  SignIn({this.toggleView});
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  final AuthService _auth = AuthService();
  //text field state
  String email = '';
  String password = '';
  String error= "";
  bool loading=false;
  String resetEmail = "";
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return loading ? Loading(): Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
        title: Text('RPG Tavern'),
        actions: <Widget>[
          FlatButton.icon(
            icon: Icon(Icons.person, color: Colors.white,),
            label: Text("Register", style: TextStyle(color: Colors.white)),
            onPressed: () => widget.toggleView(),
          )
        ],
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 0.0),
          alignment: Alignment.center,
          color: Theme.of(context).backgroundColor,
          width: MediaQuery.of(context).size.width < 500 ? double.infinity: 500,
          child: Form(
            key:_formKey,
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Text("Sign In", style: Theme.of(context).textTheme.headline,),
                SizedBox(height: 10,),
                TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) => value.isEmpty ? "Enter an E-mail.": null,
                  onChanged: (value){
                    email = value;
                  },
                  decoration: TextInputDecoration.copyWith(hintText: "Email"),
                ),
                SizedBox(height: 20,),
                TextFormField(
                  validator: (value) => value.length < 6 ? "Enter a password 6+ characters long." : null,
                  obscureText: true,
                  onChanged: (value){
                    password = value;
                  },
                  decoration: TextInputDecoration.copyWith(hintText: "Password"),
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                      color: Colors.white,
                      elevation: 10.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20.0))
                      ),
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text("Reset Password"),
                              content: TextFormField(
                                initialValue: resetEmail,
                                decoration: TextInputDecoration.copyWith(hintText: "Accout Password"),
                                onChanged: (value) => setState(()=> resetEmail = value),
                              ),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text(
                                    "Cancel",
                                    style: TextStyle(
                                      color: Colors.black54
                                    ),
                                  ),
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20.0))),
                                  onPressed: () => Navigator.of(context).pop(),
                                ),
                                FlatButton(
                                  child: Text(
                                    "Submit",
                                    style: TextStyle(
                                      color: Colors.white
                                    ),  
                                  ),
                                  color: Theme.of(context).primaryColor,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20.0))),
                                  onPressed: () async {
                                    await _auth.resetPassword(resetEmail);
                                    Navigator.of(context).pop();
                                  },
                                )                              ],
                            );
                          }
                        );
                      },
                      child: Text(
                        "Forgot Password"
                      )
                    ),
                    SizedBox(width: 20,),
                    RaisedButton(
                      color: Theme.of(context).accentColor,
                      elevation: 10.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20.0))
                      ),
                      onPressed: () async {
                        if(_formKey.currentState.validate()){
                          setState(() => loading = true);
                          dynamic result = await _auth.signInWithEmailAndPassword(email, password);
                          if(result is String){
                            setState(() {
                              loading = false;
                              error = result;
                            });
                          }
                        }
                      },
                      child: Text(
                        "Sign In",
                        style: TextStyle(color: Colors.white)
                      )
                    ),
                  ],
                ),
                SizedBox(height: 20,),
                Text(
                  error,
                  style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ],
            ),
          ),
        ),
      )
    );
  }
}
import 'package:flutter/material.dart';
import 'package:rpg_tavern_app/models/game.dart';

class ChannelForm extends StatefulWidget {
  ChannelForm({Key key}) : super(key: key);

  @override
  _ChannelFormState createState() => _ChannelFormState();
}

class _ChannelFormState extends State<ChannelForm> {

  
  @override
  Widget build(BuildContext context) {
    final Game game = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text("New Channel", style:TextStyle(color: Colors.white)),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body:Container(child: Text(game.name),)
    );
  }
}
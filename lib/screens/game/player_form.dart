import 'package:flutter/material.dart';
import 'package:rpg_tavern_app/models/game.dart';

class PlayerForm extends StatefulWidget {
  PlayerForm({Key key}) : super(key: key);

  @override
  _PlayerFormState createState() => _PlayerFormState();
}

class _PlayerFormState extends State<PlayerForm> {

  
  @override
  Widget build(BuildContext context) {
    final Game game = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text("New Attribute", style:TextStyle(color: Colors.white)),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body:Container(child: Text(game.name),)
    );
  }
}
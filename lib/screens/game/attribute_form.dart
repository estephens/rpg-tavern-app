import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:rpg_tavern_app/models/game.dart';
import 'package:rpg_tavern_app/services/database.dart';
import 'package:rpg_tavern_app/shared/constants.dart';

class AttributeForm extends StatefulWidget {
  AttributeForm({Key key}) : super(key: key);

  @override
  _AttributeFormState createState() => _AttributeFormState();
}

class _AttributeFormState extends State<AttributeForm> {

  GameAttribute attr;
  List<String> skills = [];
  Game game;
  GlobalKey _formKey = new GlobalKey();
  String newSkill ='';
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _skillController = new TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    final dynamic params = ModalRoute.of(context).settings.arguments;
    game = params['game'];
    attr = params['value'] != null ? params['value'] : new GameAttribute(aid: '', name:'', skills:[]);
    if(skills.length == 0){
      attr.skills.forEach((skill){
        skills.add(skill.toString());
      });
    }
    if(_nameController.text.isEmpty){
      _nameController.text = attr.name;
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(attr.name.isEmpty ? "New Attribute" : attr.name, style:TextStyle(color: Colors.white)),
        backgroundColor: Theme.of(context).primaryColor,
        actions: <Widget>[
          FlatButton.icon(
            icon: Icon(Icons.save, color: Colors.white),
            label: Text(
              "Save", 
              style: Theme.of(context).textTheme.subtitle.copyWith(color: Colors.white)
            ),
            onPressed: () async {
              DocumentReference attrRef;
              try{
                if(attr.aid.isEmpty){
                  attrRef = await DatabaseService().addAttribute(game.gid, _nameController.text, skills);
                }
                else{
                  attrRef = await DatabaseService().updateAttribute(game.gid, attr.aid, _nameController.text, skills);
                }
                Navigator.pop(context, new GameAttribute(aid: attrRef.documentID, name: _nameController.text, skills: skills));
              }catch(e){ print(e);}
            },
          )
        ],
      ),
      body:Container(
        padding: EdgeInsets.all(8.0),
        width: double.infinity,
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Text("Attribute Definition", style: Theme.of(context).textTheme.headline),
              SizedBox(height: 5,),
              TextFormField(
                controller: _nameController,
                validator: (value) => value.isEmpty ? "Character Attributes require a Name" : null,
                onChanged: (value) => setState(()=> attr.name = value),
                decoration: TextInputDecoration.copyWith(labelText: "Attribute Name"),
              ),
              SizedBox(height: 20,),
              Text("Skill List", style: Theme.of(context).textTheme.headline),
              SizedBox(height: 5,),
              TextFormField(
                controller: _skillController,
                onChanged: (value) => setState(()=> newSkill = value),
                onFieldSubmitted: (value){
                  if(value.isNotEmpty){
                  skills.add(value);
                  }
                  _skillController.clear();
                  newSkill = '';
                  setState((){});
                },
                decoration: TextInputDecoration.copyWith(labelText: "Skill Name"),
              ),
              RaisedButton(
              child: Text("Add Skill", style: TextStyle(color: Colors.white),),
              elevation: 10.0,
              color: Theme.of(context).primaryColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))
              ),
              onPressed: (){
                if(newSkill.isNotEmpty){
                  skills.add(newSkill);
                }
                _skillController.clear();
                newSkill = '';
                setState((){});
              },
            ),
            Expanded(
              child: new ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: skills.length,
                itemBuilder: (context, index){
                  return Dismissible(
                    key: Key(skills[index]),
                    direction: DismissDirection.endToStart,
                    background: Container(
                      padding: EdgeInsets.all(8.0),
                      alignment: Alignment.centerRight,
                      color: Colors.red,
                      child: Icon(Icons.delete, color: Colors.white),
                    ),
                    onDismissed: (direction) => skills.removeAt(index),
                    child: Container(
                      height: 60.0,
                      decoration: BoxDecoration(
                        border: Border(
                          top: BorderSide(color: Theme.of(context).accentColor, width: 1)
                        ),
                      ),
                      child: Center(
                        child:Text(skills[index])
                      ),
                    )
                  );
                },
              )
            )
            ],
          )
        ),
      )
    );
  }
}
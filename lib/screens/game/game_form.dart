import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dashed_container/dashed_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rpg_tavern_app/models/user.dart';
import 'package:rpg_tavern_app/services/database.dart';
import 'package:rpg_tavern_app/services/storage.dart';
import 'package:rpg_tavern_app/shared/constants.dart';
import 'package:rpg_tavern_app/shared/loading.dart';

class GameForm extends StatefulWidget {
  GameForm({Key key}) : super(key: key);

  @override
  _GameFormState createState() => _GameFormState();
}

class _GameFormState extends State<GameForm> {

  //Model Attributes
  String name='';
  String description='';
  String system='';
  bool public=false;
  int maxPlayers;
  String icon='';
  File iconFile;
  String header='';
  File headerFile;

  final List<String> _systems = ["Dungeons and Dragons 5e", "Path finder"];
  final List<int> _playerCounts = new List.generate(8, (i) => i+1);

  //State objects
  final _formKey = GlobalKey<FormState>();
  ScrollController _scrollController = new ScrollController();
  double _currentPosition = 0;
  bool _loading = false;

  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    super.initState();
  }

  void _scrollListener(){
    setState(() => _currentPosition = _scrollController.offset);
  }

  void _scroll(double direction){
    double screenWidth = MediaQuery.of(context).size.width * direction;
    _scrollController.animateTo(_currentPosition + screenWidth, duration:Duration(milliseconds: 1000), curve: Curves.easeOutQuint);
    setState(() => _currentPosition = _currentPosition + screenWidth);
    return;
  }
  
  Future chooseIcon() async { 
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      iconFile = image;
    });
  }

  Future chooseHeader() async { 
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      headerFile = image;
    });
  }  

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    return Scaffold(
        appBar: AppBar(
          title: Text("Create Game"),
          backgroundColor: Theme.of(context).primaryColor,
        ),
        body: _loading ? Loading() : Center(
         child: Container(
           alignment: Alignment.center,
           width: MediaQuery.of(context).size.width < 500 ? double.infinity: 500,
           color: Theme.of(context).backgroundColor,
           child: Form(
            key: _formKey,
            child: ListView(
              controller: _scrollController,
              physics: PageScrollPhysics(),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(vertical: 0, horizontal: 15.0),
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      Text("Name & Description", style: Theme.of(context).textTheme.headline),
                      SizedBox(height: 10,),
                      TextFormField(
                        initialValue: name,
                        validator: (value) => value.isEmpty ? "Game title is required" : null,
                        onChanged: (value) => setState(()=> name = value),
                        decoration: TextInputDecoration.copyWith(
                          labelText: "Game Title",
                        ),
                      ),
                      SizedBox(height: 20.0,),
                      TextFormField(
                        maxLines: 10,
                        minLines: 10,
                        textAlign: TextAlign.start,
                        textAlignVertical: TextAlignVertical.top,
                        validator: (value) => value.isEmpty && public ? "Public games require a description" : null,
                        initialValue: name,
                        onChanged: (value) => setState(()=> description = value),
                        decoration: TextInputDecoration.copyWith(labelText: "Game Description"),
                      ),
                      SizedBox(height: 20,),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          RaisedButton(
                            child: Text("Next", style: TextStyle(color: Colors.white),),
                            elevation: 10.0,
                            color: Theme.of(context).primaryColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(Radius.circular(20.0))
                            ),
                            onPressed: () => _scroll(1.0),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(vertical:0, horizontal: 15.0),
                  child: Center(
                    child: ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        Text("System & Players", style: Theme.of(context).textTheme.headline),
                        SizedBox(height: 10,),
                        DropdownButtonFormField(
                          validator: (value) => value.toString().isEmpty || value == null ? "Select a game system" : null,
                          decoration:  TextInputDecoration.copyWith(
                            contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                            labelText: "Game System"
                          ),
                          onChanged: (value) => setState(()=> system = value),
                          value: system.isEmpty ? null : system,
                          items: _systems.map((_system){
                            return DropdownMenuItem(
                              value: _system,
                              child: Text(_system),
                            );
                          }).toList()
                        ),
                        SizedBox(height: 20.0,),
                        DropdownButtonFormField(
                          validator: (value) => value == null ? "Max players is required" : null,
                          decoration:  TextInputDecoration.copyWith(
                            contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                            labelText: "Max Players"
                          ),
                          onChanged: (value) => setState(()=> maxPlayers = value),
                          value: maxPlayers ?? 1,
                          items: _playerCounts.map((_count){
                            return DropdownMenuItem(
                              value: _count,
                              child: Text(_count == 1? _count.toString() + " Player": _count.toString() + " Players")
                            );
                          }).toList()
                        ),
                        SizedBox(height: 20.0,),
                        Container(
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey, width: 2),
                            borderRadius: BorderRadius.all(Radius.circular(30.0)),
                            color: Colors.white
                          ),
                          child: CheckboxListTile(
                            title: Text("Public"),
                            selected: public,
                            value: public,
                            onChanged: (value)=> setState(()=> public = value)
                          ),
                        ),
                        SizedBox(height: 20.0,),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            RaisedButton(
                              child: Text("Back", style: TextStyle(color: Colors.black54),),
                              elevation: 10.0,
                              color: Theme.of(context).backgroundColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20.0))
                              ),
                              onPressed: () => _scroll(-1.0),
                            ),
                            RaisedButton(
                              child: Text("Next", style: TextStyle(color: Colors.white),),
                              elevation: 10.0,
                              color: Theme.of(context).primaryColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20.0))
                              ),
                              onPressed: () => _scroll(1.0),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(vertical:0, horizontal: 15.0),
                  child: Center(
                    child:ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        Text("Header & Icon", style: Theme.of(context).textTheme.headline),
                        SizedBox(height: 10.0,),
                        Container(
                          width: double.infinity,
                          child: headerFile == null ?
                            Column(
                              children: <Widget>[
                                ButtonTheme(
                                  minWidth: double.infinity,
                                  child: RaisedButton.icon(
                                    label: Text("Upload Header", style: TextStyle(color: Colors.white),),
                                    color: Theme.of(context).primaryColor,
                                    icon: Icon(Icons.photo_library, color: Colors.white),
                                    elevation: 10,
                                    onPressed: chooseHeader,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0)
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5,),
                                DashedContainer(
                                  dashColor: Colors.grey[500],
                                  dashedLength: 10.0,
                                  blankLength: 2.5,
                                  strokeWidth: 3.0,
                                  borderRadius: 25.0,
                                  boxShape: BoxShape.rectangle,
                                  child: SizedBox(
                                    height: 150,
                                    width: double.infinity,
                                    child: Center(
                                      child: Text(
                                        "Header",
                                        style: TextStyle(
                                          color: Colors.grey[600],
                                          fontSize: 20.0
                                        ),
                                      )
                                    ),
                                  )
                                ),
                              ],
                            )
                            : Column(
                              children: <Widget>[
                                ButtonTheme(
                                  minWidth: double.infinity,
                                  child: RaisedButton.icon(
                                    label: Text("Remove Header"),
                                    color: Theme.of(context).backgroundColor,
                                    icon: Icon(Icons.close),
                                    elevation: 10,
                                    onPressed: () => setState(()=> headerFile = null),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0)
                                    ),
                                  ),
                                ),
                                SizedBox(height: 10,),
                                Container(
                                  width: double.infinity,
                                  height: 150,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(25.0),
                                    child: Image.file(
                                      headerFile,
                                      width: double.infinity,
                                      fit: BoxFit.cover,
                                      alignment: Alignment.center,
                                    )
                                  ),
                                ),
                              ],
                            ),
                      ),
                      SizedBox(height: 20.0,),
                      Container(
                        width: double.infinity,
                        child: iconFile == null ?
                          Column(
                            children: <Widget>[
                              ButtonTheme(
                                minWidth: double.infinity,
                                child: RaisedButton.icon(
                                  label: Text("Upload Icon", style: TextStyle(color: Colors.white)),
                                  color: Theme.of(context).primaryColor,
                                  icon: Icon(Icons.photo_library, color: Colors.white),
                                  elevation: 10,
                                  onPressed: chooseIcon,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0)
                                  ),
                                ),
                              ),
                              SizedBox(height: 10,),
                              DashedContainer(
                                dashColor: Colors.grey[500],
                                dashedLength: 10.0,
                                blankLength: 2.5,
                                strokeWidth: 3.0,
                                borderRadius: 30.0,
                                boxShape: BoxShape.rectangle,
                                child: SizedBox(
                                  height: 60,
                                  width: 60,
                                  child: Center(
                                    child: Text(
                                      "Icon",
                                      style: TextStyle(
                                        color: Colors.grey[600],
                                        fontSize: 20.0
                                      ),
                                    )
                                  ),
                                )
                              ),
                            ],
                          )
                          : Column(
                            children: <Widget>[
                              ButtonTheme(
                                minWidth: double.infinity,
                                child: RaisedButton.icon(
                                  label: Text("Remove Icon"),
                                  color: Theme.of(context).backgroundColor,
                                  icon: Icon(Icons.close),
                                  elevation: 10,
                                  onPressed: () => setState(()=> iconFile = null),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0)
                                  ),
                                ),
                              ),
                              SizedBox(height: 10.0,),
                              CircleAvatar(
                                backgroundImage: Image.file(iconFile).image,
                                backgroundColor: Theme.of(context).backgroundColor,
                                radius: 30,
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 20.0,),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            RaisedButton(
                              child: Text("Back", style: TextStyle(color: Colors.black54),),
                              elevation: 10.0,
                              color: Theme.of(context).backgroundColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20.0))
                              ),
                              onPressed: () => _scroll(-1.0),
                            ),
                            RaisedButton(
                              child: Text("Save", style: TextStyle(color: Colors.white),),
                              elevation: 10.0,
                              color: Theme.of(context).primaryColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(20.0))
                              ),
                              onPressed: () async {
                                if(_formKey.currentState.validate()){
                                  setState(() => _loading = true);
                                  DocumentReference docRef = await DatabaseService(uid: user.uid).addGame(user.uid, name, description, system, public, maxPlayers);
                                  String headerUrl = await StorageService().addOrUpdateGameHeader(headerFile, docRef.documentID, currentFileUrl: "");
                                  String iconUrl = await StorageService().addOrUpdateGameIcon(iconFile, docRef.documentID, currentFileUrl: "");
                                  docRef = await DatabaseService(uid: user.uid).updateGame(docRef.documentID, user.uid, name, description, system, "Pending", public, maxPlayers, iconUrl, headerUrl, [user.uid]);
                                  Navigator.of(context).pop();
                                  setState(() => _loading = false);
                                }
                                else{
                                  _scroll(-2.0);
                                }
                              },
                            ),
                          ],
                        )
                      ],
                    )
                  ),
                )
                ],
              )
            )
         ),
      ),
    );
  }
}
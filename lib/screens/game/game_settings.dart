import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rpg_tavern_app/models/game.dart';
import 'package:rpg_tavern_app/screens/game/attribute_form.dart';
import 'package:rpg_tavern_app/screens/game/player_form.dart';
import 'package:rpg_tavern_app/services/database.dart';

class GameSettings extends StatefulWidget {
  GameSettings({Key key}) : super(key: key);

  @override
  _GameSettignsState createState() => _GameSettignsState();
}

class _GameSettignsState extends State<GameSettings> {
  List<GameAttribute> attrs = [];
  List<Channel> channels = [];
  String _channelName ='';

  @override
  Widget build(BuildContext context) {
    Game game = ModalRoute.of(context).settings.arguments;
    if(attrs.length == 0){
      if(game.attributes.first is GameAttribute) attrs = game.attributes;
      else attrs = game.attributes.map((attribute) => new GameAttribute(aid: attribute['aid'], name: attribute['name'], skills: attribute['skills'])).toList();
    }
    if(channels.length == 0){
      if(game.channels.first is Channel) channels = game.channels;
      else channels = game.channels.map((channel) => new Channel(cid: channel['cid'], name: channel['name'], order: channel['order'])).toList();
    }
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon:Icon(Icons.arrow_back_ios),
          onPressed:() {
            game.attributes = attrs;
            game.channels = channels;
            Navigator.pop(context, game);
          },
        ),
        title: Text("Game Settings", style: Theme.of(context).textTheme.title),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: ListView.builder(
        itemCount: channels.length + attrs.length + game.users.length + 3,
        itemBuilder: (context, index){
          final int channelStart = 1;
          final int playerStart = channels.length + 1;
          final int attrStart = game.users.length + playerStart +1;
          if(index == 0){
            return Container(
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                border: Border(
                  top: BorderSide(width: 1, color: Theme.of(context).primaryColor),
                  bottom:BorderSide(width: 1, color: Theme.of(context).primaryColor),
                )
              ),
              child:ListTile(
                title: Text('Channels', style: Theme.of(context).textTheme.headline.copyWith(color: Theme.of(context).primaryColor), ),
                isThreeLine: false,
                dense: true,
                contentPadding: EdgeInsets.symmetric(vertical: 3.0, horizontal: 10.0),
                trailing: Icon(Icons.add, color: Theme.of(context).primaryColor, size: 36,),
                onTap: () {
                  return showCupertinoDialog(
                    context: context,
                    builder: (builder){
                      return  CupertinoAlertDialog(
                        title: Text("Channel Name"),
                        content: CupertinoTextField(
                          onChanged: (value){
                            setState(() {
                              _channelName = value;
                            });
                          },
                        ),
                        actions: <Widget>[
                          CupertinoDialogAction(
                            child:Text("Cancel"),
                            onPressed: () => Navigator.pop(context),
                          ),
                          CupertinoDialogAction(
                            isDefaultAction: true,
                            child: Text("Submit"),
                            onPressed: () async {
                              if(_channelName.isNotEmpty){
                                DocumentReference channelRef = await DatabaseService().addChannel(game.gid, _channelName, channels.length);
                                Channel _newChannel = new Channel(cid: channelRef.documentID, name: _channelName, order: channels.length);
                                setState(() {
                                  channels.add(_newChannel);
                                });
                                Navigator.pop(context);
                              }
                            },
                          )
                        ],
                      );
                    }
                  );
                },
              )
            );
          }
          if(index > 0 && index < playerStart){
            return Dismissible(
              key: Key(channels[index-channelStart].cid),
              direction: DismissDirection.endToStart,
              background: Container(
                alignment: Alignment.centerRight,
                color: Colors.red,
                child: Icon(Icons.delete, color: Colors.white),
              ),
              onDismissed: (direction){
                DatabaseService().deleteChannel(game.gid, channels[index-channelStart].cid);
                channels.where((channel) => channel.order > channels[index-channelStart].order).forEach((channel) => setState(()=> channel.order = channel.order -1));
                channels.removeAt(index-channelStart);
              },
              child: ListTile(
                title: Text(
                  channels[index-channelStart].name,
                  style: Theme.of(context).textTheme.subtitle,
                ),
                trailing: Icon(Icons.reorder),
              ),
            );
          }
          if(index == playerStart){
            return Container(
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                border: Border(
                  top: BorderSide(width: 1, color: Theme.of(context).primaryColor),
                  bottom:BorderSide(width: 1, color: Theme.of(context).primaryColor),
                )
              ),
              child:ListTile(
                title: Text('Players', style: Theme.of(context).textTheme.headline.copyWith(color: Theme.of(context).primaryColor), ),
                isThreeLine: false,
                dense: true,
                contentPadding: EdgeInsets.symmetric(vertical: 3.0, horizontal: 10.0),
                trailing: Icon(Icons.add, color: Theme.of(context).primaryColor, size: 36,),
                onTap: () async {
                  final dynamic result = await Navigator.push(context, 
                    new MaterialPageRoute(
                      builder: (context) => new PlayerForm(),
                      settings: RouteSettings(
                        arguments: {
                          'game': game,
                          'value':null
                        }
                      ),
                    ),
                  );
                },
              )
            );
          }
          if(index > playerStart && index < attrStart){
            return ListTile(
              title: Text(
                game.users[index-(playerStart+1)],
                style: Theme.of(context).textTheme.subtitle,
              ),
              trailing: Icon(Icons.arrow_forward_ios),
            );
          }
          if(index == attrStart){
            return Container(
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                border: Border(
                  top: BorderSide(width: 1, color: Theme.of(context).primaryColor),
                  bottom:BorderSide(width: 1, color: Theme.of(context).primaryColor),
                )
              ),
              child:ListTile(
                title: Text('Attributes', style: Theme.of(context).textTheme.headline.copyWith(color: Theme.of(context).primaryColor), ),
                isThreeLine: false,
                dense: true,
                contentPadding: EdgeInsets.symmetric(vertical: 3.0, horizontal: 10.0),
                trailing: Icon(Icons.add, color: Theme.of(context).primaryColor, size: 36,),
                onTap: () async {
                  final dynamic result = await Navigator.push(context, 
                    new MaterialPageRoute(
                      builder: (context) => new AttributeForm(),
                      settings: RouteSettings(
                        arguments: {
                          'game': game,
                          'value':null
                        }
                      ),
                    ),
                  );
                  if(result is GameAttribute){
                    int existingIndex = attrs.indexWhere((attribute)  => attribute.aid== result.aid);
                    if(existingIndex == -1){ attrs.add(result);}
                    else{attrs[existingIndex] = result;}
                  }
                },
              )
            );
          }
          else{
            return Dismissible(
              key: Key(attrs[index-(attrStart+1)].aid),
              direction: DismissDirection.endToStart,
              background: Container(
                alignment: Alignment.centerRight,
                color: Colors.red,
                child: Icon(
                  Icons.delete, 
                  color: Colors.white,
                )
              ),
              onDismissed: (direction) async {
                await DatabaseService().deleteAttribute(game.gid, attrs[index-(attrStart+1)].aid);
                attrs.removeAt(index-(attrStart+1));
              },
              child: ListTile(
                title: Text(
                  attrs[index - (attrStart+1)].name,
                  style: Theme.of(context).textTheme.subtitle,
                ),
                trailing: Icon(Icons.arrow_forward_ios),
                onTap: () async {
                  final dynamic result = await Navigator.push(context, 
                    MaterialPageRoute(
                      builder: (context) => AttributeForm(),
                      settings: RouteSettings(
                        arguments: {
                          'game': game,
                          'value': attrs[index - (attrStart+1)]
                        }
                      ),
                    ),
                  );
                  if(result is GameAttribute){
                    int existingIndex = attrs.indexWhere((attribute)  => attribute.aid== result.aid);
                    if(existingIndex == -1){ attrs.add(result);}
                    else{attrs[existingIndex] = result;}
                  }
                },
              ),
            );
          }
        }
      ),
    );
  }
}
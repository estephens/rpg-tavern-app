import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import 'package:rpg_tavern_app/models/game.dart';
import 'package:rpg_tavern_app/models/user.dart';
import 'package:rpg_tavern_app/screens/game/game_settings.dart';
import 'dart:math' as math;

class GameHome extends StatefulWidget {
  
  GameHome({Key key}) : super(key: key);

  @override
  _GameHomeState createState() => _GameHomeState();
}

class _GameHomeState extends State<GameHome> {
  List<Channel> _channels = [];
  Game game;
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    game = ModalRoute.of(context).settings.arguments;
    if(_channels.length == 0) _channels = game.channels.map((channel) => new Channel(cid: channel['cid'], name: channel['name'], order: channel['order'])).toList();
    _channels.sort((a, b) => a.order.compareTo(b.order));
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverPersistentHeader(
              delegate: GameAppBar(expandedHeight: (_channels.length.clamp(0, 4)+1)*60.0, parent: this),
              pinned: true,
              floating: true,
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (context, index){
                  return ListTile(
                    title:Text("tile $index")
                  );
                }
              ),
            )
          ],
        ),
      ),
    );
  }
}

class GameAppBar extends SliverPersistentHeaderDelegate {
  final double expandedHeight;
  _GameHomeState parent;

  GameAppBar({@required this.expandedHeight, @required this.parent});

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent){
    return Column(
      children: <Widget>[
        AppBar(
          elevation: 5,
          centerTitle: true,
          title: Text(parent.game.name, style: TextStyle(color: Colors.white)),
          backgroundColor: Theme.of(context).primaryColor,
          leading: Icon(Icons.arrow_back_ios, color: Colors.white,),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings, color: Colors.white),
              onPressed: () async{
                dynamic result = await Navigator.push(
                  context, 
                  MaterialPageRoute(
                    builder: (context) => GameSettings(),
                      settings: RouteSettings(
                        arguments: parent.game,
                      ),
                  ),
                );
                if(result is Game){
                    parent.game = result;
                    parent._channels = parent.game.channels;
                }
              },
            )
          ],
        ),
        Expanded(
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemCount: parent._channels.length,
            itemBuilder: (context, index){
              return Container(
                width: double.infinity,
                height: 60.0,
                decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  border: Border(
                    bottom: BorderSide(width: 1, color: Theme.of(context).accentColor)
                  )
                ),
                child: Center(
                  child: Text(
                    parent._channels[index].name,
                    style: Theme.of(context).textTheme.headline.copyWith(color: Theme.of(context).primaryColor, fontSize: 20)
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

   @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}
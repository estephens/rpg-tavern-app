import 'package:flutter/material.dart';
import 'package:rpg_tavern_app/models/character.dart';

class CharacterTile extends StatelessWidget {
  final Character character;
  CharacterTile({this.character});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.brown,
            radius: 30,
          ),
          title: Text(
            character.name
          ),
          subtitle: Text(character.race + " " + character.cClass),
        ),
      ),
    );
  }
}
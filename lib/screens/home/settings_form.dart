import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:rpg_tavern_app/services/auth.dart';
import 'package:rpg_tavern_app/services/database.dart';
import 'package:rpg_tavern_app/models/user.dart';
import 'package:rpg_tavern_app/services/storage.dart';
import 'package:rpg_tavern_app/shared/avatar.dart';
import 'package:rpg_tavern_app/shared/constants.dart';
import 'package:rpg_tavern_app/shared/loading.dart';

class SettingsForm extends StatefulWidget {
  SettingsForm({Key key}) : super(key: key);

  @override
  _SettingsFormState createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {
  final _formKey = GlobalKey<FormState>();

  //form values
  String _currentUsername;
  int _currentColor;
  String _currentAvatar;
  File _avatarFile;
  String _deleteUsername;
  Color avatarColor;

  //control bits
  bool _isSaving = false;

  //avatar picker
  Future chooseAvatar() async { 
    File image = await ImagePicker.pickImage(source: ImageSource.gallery, maxWidth: 250);
    setState(() {
      _avatarFile = image;
    });
  }
  
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData,
      builder: (context, snapshot) {
        if(snapshot.hasData){
          UserData userData = snapshot.data;
          if(_currentColor == null){
            _currentColor = userData.colorData;
          }
          avatarColor = Color(_currentColor);
          if(_currentAvatar == null){
            _currentAvatar = userData.avatar;
          }
          return Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.delete_forever, color: Colors.red,),
                      onPressed: (){
                        Navigator.of(context).pop();
                        showCupertinoModalPopup(
                          context: context,
                          builder: (BuildContext context) {
                            final _username = userData.username ?? _currentUsername;
                            return CupertinoAlertDialog(
                              title: Text("Delete User Account"),
                              content: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    "A deleted account is removed from all servers and characters are deleted. \n\n To delete the account type the user name ($_username) below.",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.red),
                                  ),
                                  SizedBox(height: 10.0,),
                                  CupertinoTextField(
                                    onSubmitted: (value) async {
                                       if(_username == value){
                                        await StorageService().deleteImage(userData.avatar);
                                        final _auth = AuthService();
                                        _auth.deleteAccount();
                                      }
                                    Navigator.of(context).pop();
                                    },
                                    onChanged: (val){
                                      _deleteUsername = val;
                                    },
                                  ),
                                  SizedBox(height: 10.0,),
                                ],
                              ),
                              actions: <Widget>[
                                CupertinoDialogAction(
                                  child: Text("Cancel", style: TextStyle(color: Colors.blue)),
                                  onPressed: () => Navigator.of(context).pop(),
                                ),
                                CupertinoDialogAction(
                                  child: Text("Delete", style: TextStyle(color: Colors.red),),
                                  onPressed: ()async{
                                    if(_username == _deleteUsername){
                                      await StorageService().deleteImage(userData.avatar);
                                      final _auth = AuthService();
                                      _auth.deleteAccount();
                                    }
                                    Navigator.of(context).pop();
                                  }
                                )
                              ],
                            );
                          }
                        );
                      },
                    ),
                    Text(
                      "Settings",
                      style: Theme.of(context).textTheme.headline
                    ),
                    IconButton(
                      icon: Icon(Icons.save),
                      onPressed: () async {
                      if(_formKey.currentState.validate()){
                        setState(() => _isSaving = true);
                        String url = '';
                        if(_avatarFile != null){
                          url = await StorageService().addOrUpdateUser(_avatarFile, user.uid, currentFileUrl: userData.avatar);
                        }
                        else if(_currentAvatar.isEmpty && userData.avatar.isNotEmpty){
                          await StorageService().deleteImage(userData.avatar);
                        }
                        if(url.isNotEmpty && url is String){
                          setState(() => _currentAvatar = url);
                        }
                        await DatabaseService(uid: userData.uid).updateUserData(
                          _currentUsername ?? userData.username, 
                          Theme.of(context).primaryColor.value, 
                          _currentAvatar ?? userData.avatar
                        );
                        setState(()=> _isSaving = false);
                        Navigator.of(context).pop();
                        }
                      },
                    ),
                  ],
                ),
                ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  children: _isSaving ? <Widget>[Center(child:Loading())] : <Widget>[
                    TextFormField(
                      initialValue: userData.username,
                      decoration: TextInputDecoration.copyWith(labelText:"Username"),
                      validator: (val) => val.isEmpty ? "Please enter a username": null,
                      onChanged: (val) => setState(() => _currentUsername = val),
                    ),
                    SizedBox(height: 20,),
                  Container(
                    width: double.infinity,
                    child: _avatarFile == null ?
                      _currentAvatar.isEmpty?
                      Column(
                        children: <Widget>[
                          ButtonTheme(
                            minWidth: double.infinity,
                            child: RaisedButton.icon(
                              label: Text("Upload Avatar", style: TextStyle(color: Colors.white)),
                              color: Theme.of(context).primaryColor,
                              icon: Icon(Icons.photo_library, color: Colors.white),
                              elevation: 10,
                              onPressed: chooseAvatar,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)
                              ),
                            ),
                          ),
                          SizedBox(height: 5,),
                          OptionalAvatar(
                            color: Theme.of(context).primaryColor.value,
                            name: _currentUsername ?? userData.username
                          )
                        ],
                      ):
                      Column(
                        children: <Widget>[
                          ButtonTheme(
                            minWidth: double.infinity,
                            child: RaisedButton.icon(
                              label: Text("Remove Avatar", style: TextStyle(color: Colors.black54)),
                              color: Theme.of(context).backgroundColor,
                              icon: Icon(Icons.close, color: Colors.black54),
                              elevation: 10,
                              onPressed: () => setState(()=> _currentAvatar = ''),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)
                              ),
                            ),
                          ),
                          SizedBox(height: 5,),
                          CircleAvatar(
                            backgroundColor: Theme.of(context).backgroundColor,
                            backgroundImage: Image.network(_currentAvatar).image,
                            radius: 30,
                          )
                        ],
                      ):
                      Column(
                        children: <Widget>[
                          ButtonTheme(
                            minWidth: double.infinity,
                            child: RaisedButton.icon(
                              label: Text("Remove Avatar", style: TextStyle(color: Colors.black54)),
                              color: Theme.of(context).backgroundColor,
                              icon: Icon(Icons.close, color: Colors.black54),
                              elevation: 10,
                              onPressed: () => setState(()=> _avatarFile = null),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)
                              ),
                            ),
                          ),
                          SizedBox(height: 5,),
                          CircleAvatar(
                            backgroundImage: Image.file(_avatarFile).image,
                            radius: 30,
                          )
                        ],
                      ),
                    ),
                  ]
                ),
              ],
            )
          );
        }
        else{
          return Loading();
        }
      }
    );
  }
}
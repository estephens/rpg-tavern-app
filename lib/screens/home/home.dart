import 'package:flutter/material.dart';
import 'package:rpg_tavern_app/models/game.dart';
import 'package:rpg_tavern_app/models/message.dart';
import 'package:rpg_tavern_app/screens/game/game_home.dart';
import 'package:rpg_tavern_app/screens/game/game_form.dart';
import 'package:rpg_tavern_app/screens/home/settings_form.dart';
import 'package:rpg_tavern_app/screens/tavern/tavern.dart';
import 'package:rpg_tavern_app/services/auth.dart';
import 'package:provider/provider.dart';
import 'package:rpg_tavern_app/services/database.dart';
import 'package:rpg_tavern_app/models/user.dart';
import 'package:rpg_tavern_app/shared/loading.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final AuthService _auth = new AuthService();
  String page = "Tavern";

  void changePage(String newPage){
    setState(() {
      page = newPage;
    });
    Navigator.of(context).pop();
  }
  
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Text("The Tavern"),
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
        actions: buildActions(page)
      ),
      drawer: Container(
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
        width: MediaQuery.of(context).size.width/2,
        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          border: Border(
            left: BorderSide(
              color: Theme.of(context).accentColor,
              width: 2
              )
          )
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            UserTile(),
            FlatButton.icon(
              icon: Icon(Icons.add),
              label: Text("Start Game"),
              onPressed: (){
                Navigator.of(context).pop();
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => GameForm())
                );
              }
            ),
            FlatButton.icon(
              icon: Icon(Icons.free_breakfast),
              label: Text("The Tavern"),
              onPressed: () => changePage("Tavern"),
            ),
            Container(
              height: 0,
              width: double.infinity,
              decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Theme.of(context).accentColor, width: 1)),),
            ),
            Expanded(
              child: StreamBuilder<List<Game>>(
                stream: DatabaseService(uid: user.uid).games,
                builder: (context, snapshot){
                  if(snapshot.hasData){
                    final _games = snapshot.data;
                    return ListView(
                      children: _games.map((game){
                        return Center(
                          child: FlatButton(
                            child: Text(game.name),
                            onPressed: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => GameHome(),
                                    // Pass the arguments as part of the RouteSettings. The
                                    // DetailScreen reads the arguments from these settings.
                                    settings: RouteSettings(
                                      arguments: game,
                                    ),
                                  ),
                                );
                              },
                          ),
                        );
                      }).toList()
                    );
                  }
                  else{
                    return Loading();
                  }
                }
              )
            ),
            SafeArea(
              child: FlatButton.icon(
                icon: Icon(Icons.exit_to_app),
                label: Text("Logout"),
                onPressed: () async {
                  await _auth.signOut();
                },
              ),
            )
          ],
        ),
      ),
      body: HomeBodyWrapper(page: page),
      
    );
  }
}

class HomeBodyWrapper extends StatelessWidget {
  final String page;
  
  HomeBodyWrapper({this.page});
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    if(page == "Characters"){
      return Container(
        child: Center(child: Text("Characters")),
      );
    }
    else{
      if(user != null){
        return StreamProvider<List<Message>>.value(
          value: DatabaseService(uid: user.uid).messages,
          child: Tavern()
        );
      }
      else{
        return Loading();
      }
    }
    
  }
}

List<Widget> buildActions(String page){
  List<Widget> widgets = [];
  if(page == "Characters"){
    widgets.add(IconButton(icon: Icon(Icons.person_add),onPressed: ()=>print("Add Character")));
  }
  if(page == "Messages"){
    widgets.add(IconButton(icon: Icon(Icons.group),onPressed: ()=>print("show left drawer")));
  }

  return widgets;
}

class UserTile extends StatelessWidget {
  const UserTile({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData ,
      builder: (context, snapshot){
        if(snapshot.hasData){
          final userData = snapshot.data;
          return Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: Theme.of(context).accentColor, width: 1)),
            ),
            child:SafeArea(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  UserAvatar(userData: userData),
                  SizedBox(height: 10,),
                  FlatButton.icon(
                    icon: Icon(Icons.settings, color: Colors.black,),
                    label: Text(
                      "Settings",
                      style: TextStyle(color: Colors.black)
                    ),
                    onPressed: (){
                      Navigator.pop(context);
                      showModalBottomSheet(
                        context: context,
                        builder: (context) {
                          return Container(
                            padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 60.0),
                            child: SettingsForm()
                          );
                        }
                      );
                    },
                  ),
                ],
              ),
            ),
          );
        }
        else{
          return Loading();
        }
      }
    );
  }
}

class UserAvatar extends StatelessWidget {
  final UserData userData;

  UserAvatar({this.userData});

  @override
  Widget build(BuildContext context) {
    if(userData.avatar.isEmpty){
      return CircleAvatar(
        backgroundColor: userData.color() ?? Theme.of(context).accentColor,
        radius: 50.0,
        child: Text(
          userData.username.substring(0, 1).toUpperCase() ?? "",
          style: TextStyle(
            color: userData.color() == Colors.white ? Colors.black : Colors.white,
            fontSize: 50.0
          ),
        ),
      );
    }
    else{
      return CircleAvatar(
        backgroundColor: userData.color() ?? Theme.of(context).accentColor,
        radius: 50.0,
        backgroundImage:Image.network(userData.avatar).image
      );
    }
  }
}
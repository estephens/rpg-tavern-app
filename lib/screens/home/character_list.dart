import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rpg_tavern_app/models/character.dart';
import 'package:rpg_tavern_app/screens/home/character_tile.dart';

class CharacterList extends StatefulWidget {
  @override
  _CharacterListState createState() => _CharacterListState();
}

class _CharacterListState extends State<CharacterList> {
  @override
  Widget build(BuildContext context) {
    final List<Character> characters = Provider.of<List<Character>>(context);
    return ListView.builder(
      itemCount: characters == null ? 0 : characters.length,
      itemBuilder: (context, index){
        return CharacterTile(character: characters[index]);
      } ,
    );
  }
}
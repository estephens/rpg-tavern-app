import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rpg_tavern_app/models/message.dart';
import 'package:rpg_tavern_app/models/user.dart';
import 'package:rpg_tavern_app/screens/tavern/message_tile.dart';
import 'package:rpg_tavern_app/services/database.dart';
import 'package:rpg_tavern_app/shared/loading.dart';

class Tavern extends StatefulWidget {
  Tavern({Key key}) : super(key: key);

  @override
  _TavernState createState() => _TavernState();
}

class _TavernState extends State<Tavern> {

  final _textController = TextEditingController();
  String _messageText = '';

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserData>(context);
    if(user != null){
      final messageService = DatabaseService(uid: user.uid);
      return StreamBuilder<List<Message>>(
        stream: DatabaseService(uid: user.uid).messages,
        builder: (context, snapshot) {
          if(snapshot.hasData){
            final messages = snapshot.data;
            List<String> messageUsers = [];
            messages.forEach((message){
              if(!messageUsers.contains(message.uid)) messageUsers.add(message.uid);
            });
            return StreamBuilder<List<UserData>>(
              stream: DatabaseService(uid: user.uid).usersInList(messageUsers),
              builder: (context, userSnapshot) {
                if(userSnapshot.hasData){
                  final userdFromQuery = userSnapshot.data;
                  userdFromQuery.forEach((userFromList){
                    messages.where((message) => message.uid == userFromList.uid).forEach((message){
                      message.user = userFromList;
                    });
                  });
                  return Column(
                    children: <Widget>[
                      Expanded(
                        child: ListView.builder(
                          itemCount: messages == null ? 0 : messages.length,
                          itemBuilder: (context, index){
                            return MessageTile(message: messages[index], currentUser: user,);
                          } ,
                        ),
                      ),
                      BottomAppBar(
                        elevation: 0,
                        color: Theme.of(context).backgroundColor,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                                child: CupertinoTextField(
                                  controller: _textController,
                                  minLines: 1,
                                  maxLines: 5,
                                  textInputAction: TextInputAction.done,
                                  placeholder: "Message...",
                                  onChanged: (val) => _messageText = val,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                    border: Border.all(
                                      color: Colors.grey[600]
                                    )
                                  )
                                ),
                              ),
                            ),
                            IconButton(
                              icon: Icon(Icons.send),
                              onPressed: (){
                                String messageText = _messageText.trim();
                                if(messageText.isNotEmpty){
                                  messageService.addMessage(user.uid, "", messageText, DateTime.now(), false);
                                }
                                _textController.clear();
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  );
                }
                else {
                  return Loading();
                }
              }
            );
          }
          else{
            return Loading();
          }
        }
      );
    }
    else{
      return Loading();
    }
  }
}
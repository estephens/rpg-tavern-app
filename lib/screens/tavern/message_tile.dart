import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rpg_tavern_app/models/message.dart';
import 'package:rpg_tavern_app/services/database.dart';

import '../../models/user.dart';

class MessageTile extends StatelessWidget {
  final Message message;
  final UserData currentUser;
  MessageTile({this.message, this.currentUser});

  void _showModalDialog(BuildContext context,UserData user, Message message){
    TextEditingController _popupInputController = new TextEditingController();
    showCupertinoModalPopup(
      context: context,
      builder: (context) => CupertinoActionSheet(
      title: Text("Message Options"),
      actions: <Widget>[
        CupertinoActionSheetAction(
          child: Text("Edit"),
          onPressed: (){
            Navigator.pop(context);
            _popupInputController = new TextEditingController(text: message.text);
            showCupertinoModalPopup(
              context: context,
              builder: (context) => new CupertinoAlertDialog(
                title: Text("Edit Message"),
                content: CupertinoTextField(
                  controller: _popupInputController,
                  maxLines: 5,
                  textInputAction: TextInputAction.done,
                  onSubmitted: (text){
                    DatabaseService(uid: user.uid).updateMessage(message.mid, message.uid, message.action, text, message.time, DateTime.now(), true);
                    Navigator.pop(context);
                  },
                ),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text('Cancel'),
                    isDefaultAction: true,
                    onPressed: (){
                      Navigator.pop(context);
                    },
                  ),
                  CupertinoDialogAction
                  (
                    child: Text('Save'),
                    onPressed: (){
                      DatabaseService(uid: user.uid).updateMessage(message.mid, message.uid, message.action, _popupInputController.text, message.time, DateTime.now(), true);
                      Navigator.pop(context);
                    },
                  )
                ],
              )
            );
          },
        ),
        CupertinoActionSheetAction(
          child: Text(
            "Delete",
            style: TextStyle(
                color: Colors.red,
                fontSize: 20
            ),
          ),
          onPressed: (){
            Navigator.pop(context);
            showCupertinoModalPopup(
                context: context,
                builder: (context) => new CupertinoAlertDialog(
                  title: Text("Delete Message"),
                  content: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Are you sure you want to delete this message?',
                          style: TextStyle(
                            color: Colors.black,
                          ),
                        ),
                        Container(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(message.action,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle
                                  .copyWith(color: Colors.black
                              ),
                            ),
                            Text(message.footer(),
                              textAlign: TextAlign.left,
                              style: Theme.of(context)
                                  .textTheme
                                  .caption
                                  .copyWith(color: Colors.grey,
                              ),
                            )
                          ]
                        ),
                        Container(
                          alignment: Alignment.topLeft,
                          padding: EdgeInsets.fromLTRB(0, 5, 0, 20),
                          decoration: BoxDecoration(
                            border: Border(
                              top: BorderSide(
                                color: ThemeData.light().primaryColor,
                                width: 2
                              )
                            )
                          ),
                          child:
                            Text(
                              message.text,
                              textAlign: TextAlign.left,
                              softWrap: true,
                              style: Theme.of(context)
                                  .textTheme.body1
                                  .copyWith(color: Colors.black
                              ),
                            )
                          )
                        ],
                      ),
                    ),
                    actions: <Widget>[
                      CupertinoDialogAction(
                        child: Text('Cancel'),
                        isDefaultAction: true,
                        onPressed: () => Navigator.pop(context),
                      ),
                      CupertinoDialogAction(
                        child: Text(
                          'Delete',
                          style: TextStyle(
                            color: Colors.red
                          ),
                        ),
                        onPressed: (){
                          DatabaseService(uid: user.uid).deleteMesssage(message.mid);
                          Navigator.pop(context);
                        },
                      )
                    ],
                  )
              );
            }
          )
        ]
      )
    );
  }


    

  @override
  Widget build(BuildContext context){
    final userData = message.user;
    if(userData != null){
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          child: ListTile(
            leading: userData.avatar.isEmpty ? CircleAvatar(
              backgroundColor: userData.color(),
              radius: 30,
              child: Text(
                userData.username.substring(0,1).toUpperCase() ?? "N",
                style: TextStyle(
                  color: userData.color() == Colors.white ? Colors.black : Colors.white,
                  fontSize: 30.0
                ),
              ),
            ):
            CircleAvatar(
              backgroundColor: userData.color(),
              radius: 30,
              backgroundImage: Image.network(userData.avatar).image,
            )
            ,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  userData.username
                ),
                Text(
                  message.footer(),
                  style: TextStyle(
                    color: Colors.grey[300],
                    fontSize: 15.0
                  ),
                )
              ],
            ),
            subtitle: Text(message.text),
            onLongPress: (){
              if(message.uid == currentUser.uid){
                _showModalDialog(context, currentUser, message);
              }
            },
          ),
        ),
      );
    }
    else {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          child: ListTile(
            leading: Container(
              width: 60.0,
              height: 60.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30.0),
                gradient: LinearGradient(
                  begin: Alignment(1,0),
                  end: Alignment(-1,0),
                  colors: [Colors.black12, Colors.black26, Colors.black12]
                ),
              ),
            ),
            title: Container(
              width: 100.0,
              height: 20.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                gradient: LinearGradient(
                  begin: Alignment(1,0),
                  end: Alignment(-1,0),
                  colors: [Colors.black12, Colors.black26, Colors.black12]
                ),
              ),
            ),
            subtitle: Container(
              width: double.infinity,
              height: 20.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                gradient: LinearGradient(
                  begin: Alignment(1,0),
                  end: Alignment(-1,0),
                  colors: [Colors.black12, Colors.black26, Colors.black12]
                ),
              ),
            ),
          ),
        ),
      );
    }
  }
}
import 'package:flutter/material.dart';
import 'package:rpg_tavern_app/screens/home/home.dart';
import 'package:provider/provider.dart';
import 'package:rpg_tavern_app/screens/authenticate/authenticate.dart';
import 'package:rpg_tavern_app/models/user.dart';
import 'package:rpg_tavern_app/screens/home/verify_account.dart';
import 'package:rpg_tavern_app/services/database.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    final user = Provider.of<User>(context);
    // return either the Home or Authenticate widget
    if(user == null){
      return Authenticate();
    }
    if(!user.isVerified){
      return VerifyAccount();
    }
    else{
      var _dataService = DatabaseService(uid: user.uid);
      return StreamProvider<UserData>.value(
        value: _dataService.userData,
        child:Home()
      );
      // return Home();
    }
  }
}
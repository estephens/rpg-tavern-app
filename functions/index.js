const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();


//GAME FUNCTIONS
//UPDATE CHANNEL LIST ON CHANNEL CHANGE
exports.aggregateChannels = functions.firestore
    .document("games/{gameId}/channels/{channelId}")
    .onWrite((change, context) => {
        const gameId = context.params.gameId;
        const channelId = context.params.channelId;

        //ref parent document
        const gameRef = admin.firestore().collection('games').doc(gameId);

        return gameRef.collection('channels').orderBy('order', 'asc')
            .get()
            .then(querySnapshot => {
                const channels = [];

                querySnapshot.forEach(doc => {
                    var docData = doc.data();
                    channels.push(docData);
                });

                const data = {channels};

                return gameRef.update(data);
            })
            .catch(err => console.log(err));
    });
//UPDATE ATTRIBUTE LIST ON CHANGE
exports.aggregateAttributes = functions.firestore
    .document("games/{gameId}/attributes/{attributeId}")
    .onWrite((change, context) => {
        const gameId = context.params.gameId
        const attrId = context.params.attributeId

        //ref parent document
        const gameRef = admin.firestore().collection('games').doc(gameId);

        return gameRef.collection('attributes').orderBy('name', 'asc')
            .get()
            .then(querySnapshot => {
                const attributes = [];

                querySnapshot.forEach(doc => {
                    var docData = doc.data();
                    attributes.push(docData);
                });

                const data = {attributes};

                return gameRef.update(data);
            })
            .catch(err => console.log(err));
    });

//SAVE ATTR ID to ATTR
exports.createAttribute = functions.firestore
    .document("games/{gameId}/attributes/{attributeId}")
    .onCreate((snapshot, context) => {
        return snapshot.ref.update({'aid': snapshot.id});
    });
//SAVE Channel ID to Channel
exports.createChannel = functions.firestore
    .document("games/{gameId}/channels/{channelId}")
    .onCreate((snapshot, context) => {
        return snapshot.ref.update({ 'cid': snapshot.id });
    });

    //SAVE Channel ID to Channel
exports.reorderChannels = functions.firestore
.document("games/{gameId}/channels/{channelId}")
.onDelete((snapshot, context) => {
    const gameId = context.params.gameId
    const channelId = context.params.channelId

    //ref parent document
    const gameRef = admin.firestore().collection('games').doc(gameId);
    channelData = snapshot.data();
    return gameRef.collection('channels').where('order', '>' , channelData['order']).orderBy('order', 'asc')
        .get()
        .then(querySnapshot => {
            const channels = [];

            querySnapshot.forEach(doc => {
                var docData = doc.data();
                docData['order'] = docData['order'] - 1;
                doc.update(docData)
                channels.push(docData);
            });

            const data = {channels};

            return gameRef.update(data);
        })
        .catch(err => console.log(err));
});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
